# Clickstream

Photo website hosted on Gitlab or Github pages

1. This contains source code for hugo static site
2. Gitlab / Github is not good at storing images. Images are stored in a public google drive
3. As part of page build, a script downloads all the images to the right directory
4. This expects a variable providing archive.zip file. It can be hosted on local and served using ngrok. Change the variable value in the settings of gitlab, variables
